import React from 'react';

class Comp1 extends React.Component{
  render(){
    return(
      <div style={{color:`${this.props.color}`}} >{this.props.text}</div>
    );
  }
}

export default Comp1