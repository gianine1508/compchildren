import './App.css';
import Border from './components/children';
import Comp1 from './components/comp1';
import Comp2 from './components/comp2';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div >
          <Border title="Primeiro teste">
            <Comp1 color="red" text="Primeiro componente"></Comp1>
          </Border>
          <Border title="Segundo teste">
            <Comp2 fontWeight="900" text="Segundo componente"></Comp2>
          </Border>
        </div>
      </header>
    </div>
  );
}

export default App;
